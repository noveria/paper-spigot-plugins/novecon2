package me.linuxsquare.novecon2.controller;

import me.linuxsquare.novecon2.commands.NoveconCommand;
import me.linuxsquare.novecon2.commands.NoveconTabCompleter;
import me.linuxsquare.novecon2.listener.PlayerJoinListener;
import me.linuxsquare.novecon2.model.*;
import me.linuxsquare.realbanks2.controller.RealBanksAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/*

    NovEcon
    Copyright (C) 2021  LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to contact me:
    E-Mail: linuxsquare@noveria.org

*/

/*
=============================================================================================================

                        DISCLAIMER
    If you purchased this plugin from anywhere other than Spigot or Bukkit, 
    I regret that you were ripped off.
    Unfortunately, I can't do anything about that, 
    as the plugin is released under the GNU AGPLv3 and anyone is free (as in freedom) 
    to take the plugin and sell it, redistribute it, etc.

    However, if the purchase page did not mention a direct link to this or the public GitLab repository, 
    I would be happy if you could send me the page where you bought the plugin.

    See contact above on how you can contact me.

    Thank you in advance
    ~ LinuxSquare

=============================================================================================================
*/

public class NovEcon2 extends JavaPlugin {

    public static final String PREFIX = "§9[NovEcon2] §r";

    private EconomyModel economyModel;
    private VaultHook vaultHook;
    private DecimalFormat moneyFormat;

    private DatabaseModel databaseModel;
    private ConfigModel configModel;
    private BankUUID bankUUIDModel;
    private LanguagesModel languagesModel;

    /* Dependencies */
    private RealBanksAPI realBanksAPI;


    public void onEnable() {
        runOnEnable();
    }

    private void instanceClasses() {
        languagesModel = new LanguagesModel(this);
        economyModel = new EconomyModel(this);
        configModel = new ConfigModel(this);
        databaseModel = new DatabaseModel(this);
        bankUUIDModel = new BankUUID(this);
        vaultHook = new VaultHook(this);

        if(getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
            realBanksAPI = new RealBanksAPI();
            realBanksAPI.getDatabaseAPIModel().connect();
        }
        this.getCommand("novecon").setExecutor(new NoveconCommand(this));
        this.getCommand("novecon").setTabCompleter(new NoveconTabCompleter());
        this.getCommand("money").setExecutor(new NoveconCommand(this));
        this.getCommand("money").setTabCompleter(new NoveconTabCompleter());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoinListener(this), this);
    }

    private void runOnEnable() {
        instanceClasses();
        configModel.loadConfig();
        bankUUIDModel.loadConfig();
        languagesModel.loadConfig();
        databaseModel.connect();
        databaseModel.DatabaseRefresher();
        databaseModel.bankCorrector();
        vaultHook.hook();

        this.moneyFormat = new DecimalFormat("###,##0.00");
        moneyFormat.setRoundingMode(RoundingMode.FLOOR);
    }

    public void onDisable() {
        vaultHook.unhook();
        databaseModel.stopDBRefresher();
        databaseModel.stopBankCorrector();
        databaseModel.close();
    }

    public EconomyModel getEconomyModel() {
        return this.economyModel;
    }

    public DatabaseModel getDatabaseModel() {
        return this.databaseModel;
    }

    public ConfigModel getConfigModel() {
        return this.configModel;
    }

    public BankUUID getBankUUIDModel() {
        return this.bankUUIDModel;
    }

    public LanguagesModel getLanguagesModel() {
        return this.languagesModel;
    }

    public RealBanksAPI getRBAPI() {
        return this.realBanksAPI;
    }

    public String formatMoney(double amount) {
        String formated = moneyFormat.format(amount);
        if(amount == 1) {
            if(configModel.getConf().getString("Settings.currencypos").equalsIgnoreCase("front") && configModel.getConf().getString("Settings.currencypos") != null) {
                return configModel.getConf().getString("Settings.currency") + " " + formated;
            }

            return formated + " " + configModel.getConf().getString("Settings.currency");
        }
        return formated + " " + configModel.getConf().getString("Settings.currency");
    }

    public Double round(double amount, int places) {
        if(places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(amount));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
