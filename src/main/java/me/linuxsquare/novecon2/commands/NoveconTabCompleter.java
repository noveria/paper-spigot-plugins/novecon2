package me.linuxsquare.novecon2.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class NoveconTabCompleter implements TabCompleter {

    List<String> arguments = new ArrayList<>();

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {

        if(command.getName().equalsIgnoreCase("money")) {
            if(arguments.isEmpty()) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    arguments.add(player.getName());
                }
            }

            List<String> result = new ArrayList<>();
            if(args.length == 1) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[0].toLowerCase())) {
                        result.add(a);
                    }
                }
                return result;
            }
        }

        if(command.getName().equalsIgnoreCase("novecon")) {
            if(arguments.isEmpty()) {
                arguments.add("addbalance"); arguments.add("addbal");
                arguments.add("removebalance"); arguments.add("removebal");
                arguments.add("reload");
            }

            List<String> result = new ArrayList<>();
            if(args.length == 1) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[0].toLowerCase())) {
                        result.add(a);
                    }
                }
                return result;
            }
            if(args.length == 2) {
                for(Player a : Bukkit.getOnlinePlayers()) {
                    if(a.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
                        result.add(a.getName());
                    }
                }
            }
        }

        return null;
    }
}
