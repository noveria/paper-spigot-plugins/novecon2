package me.linuxsquare.novecon2.commands;

import me.linuxsquare.novecon2.controller.NovEcon2;
import me.linuxsquare.novecon2.model.LanguagesModel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class NoveconCommand implements CommandExecutor {

    private NovEcon2 novecon2;
    private LanguagesModel lang;

    public NoveconCommand(NovEcon2 plugin) {
        novecon2 = plugin;
        lang = novecon2.getLanguagesModel();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player) {
            Player player = (Player) sender;

            if(command.getName().equalsIgnoreCase("money")) {
                if(args.length == 0) {

                    if(!player.hasPermission("novecon2.money.self") && !player.hasPermission("novecon2.money.*")) {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                        }

                        return true;
                    }

                    try {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".Balance").replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().getBalance(player))));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.Balance").replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().getBalance(player))));
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                } else if (args.length == 1) {

                    if(!player.hasPermission("novecon2.money.others") && !player.hasPermission("novecon2.money.*")) {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                        }
                        return true;
                    }

                    try {
                        if(args[0].equalsIgnoreCase("centralbank")) {

                            if(!player.hasPermission("novecon2.money.centralbank") && !player.hasPermission("novecon2.money.*")) {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                                }
                                return true;
                            }

                            if(!novecon2.getEconomyModel().hasBankSupport()) {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".CentralBankOff"));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.CentralBankOff"));
                                }
                                return true;
                            }
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".CentralBankBalance").replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().bankBalance(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID")).balance)));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.CentralBankBalance").replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().bankBalance(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID")).balance)));
                            }
                        } else {
                            try {
                                OfflinePlayer op = Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[0]));
                                if(!op.hasPlayedBefore()) {
                                    if(lang.get().contains(player.getLocale().toLowerCase())) {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".PlayerNeverPlayed").replace("%player%", args[0]));
                                    } else {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.PlayerNeverPlayed").replace("%player%", args[0]));
                                    }
                                    return true;
                                }
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".OtherPlayerBalance").replace("%player%", args[0]).replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().getBalance(op))));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.OtherPlayerBalance").replace("%player%", args[0]).replace("%balance%", novecon2.formatMoney(novecon2.getEconomyModel().getBalance(op))));
                                }
                            } catch (Exception e) {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".PlayerNeverPlayed").replace("%player%", args[0]));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.PlayerNeverPlayed").replace("%player%", args[0]));
                                }
                            }

                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if(command.getName().equalsIgnoreCase("novecon")) {

                if(!player.hasPermission("novecon2.admin") && !player.hasPermission("novecon2.admin.*")) {
                    if(lang.get().contains(player.getLocale().toLowerCase())) {
                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                    } else {
                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                    }
                    return true;
                }

                if(args.length == 0) {
                    if(lang.get().contains(player.getLocale().toLowerCase())) {
                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNovEconCommand"));
                    } else {
                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNovEconCommand"));
                    }
                    return true;
                }

                if((args[0].equalsIgnoreCase("addbalance") || args[0].equalsIgnoreCase("addbal"))) {

                    if(!player.hasPermission("novecon2.admin.addbalance") && !player.hasPermission("novecon2.admin.*")) {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                        }
                        return true;
                    }

                    if(args.length == 3) {
                        try {
                            OfflinePlayer target = Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[1]));
                            int depamt = 0;
                            if(args[2].matches("[0-9]+")) {
                                depamt = Integer.parseInt(args[2]);
                            } else {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNumber"));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNumber"));
                                }
                                return true;
                            }


                            novecon2.getEconomyModel().depositPlayer(target, depamt);
                            if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                                novecon2.getEconomyModel().bankWithdraw(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID"), depamt);
                            }
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".DepositMessage").replace("%balance%", novecon2.formatMoney(depamt)).replace("%player%", target.getName()));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.DepositMessage").replace("%balance%", novecon2.formatMoney(depamt)).replace("%player%", target.getName()));
                            }
                        } catch(Exception e) {
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".PlayerNeverPlayed").replace("%player%", args[1]));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.PlayerNeverPlayed").replace("%player%", args[1]));
                            }
                        }
                    } else {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNovEconAddBalanceCommand"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNovEconAddBalanceCommand"));
                        }
                        return true;
                    }
                } else if((args[0].equalsIgnoreCase("removebalance") || args[0].equalsIgnoreCase("removebal"))) {

                    if(!player.hasPermission("novecon2.admin.removebalance") && !player.hasPermission("novecon2.admin.*")) {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                        }
                        return true;
                    }

                    if(args.length == 3) {
                        try {

                            OfflinePlayer target = Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[1]));
                            int depamt = 0;
                            if(args[2].matches("[0-9]+")) {
                                depamt = Integer.parseInt(args[2]);
                            } else {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNumber"));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNumber"));
                                }
                                return true;
                            }

                            if(novecon2.getEconomyModel().getBalance(Bukkit.getOfflinePlayer(target.getUniqueId())) >= depamt) {
                                novecon2.getEconomyModel().withdrawPlayer(target, depamt);
                            } else {
                                if(lang.get().contains(player.getLocale().toLowerCase())) {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NotEnoughMoneyInPocket").replace("%player%", target.getName()));
                                } else {
                                    player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NotEnoughMoneyInPocket").replace("%player%", target.getName()));
                                }
                                return true;
                            }

                            if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                                novecon2.getEconomyModel().bankDeposit(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID"), depamt);
                            }
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".RemoveMessage").replace("%balance%", novecon2.formatMoney(depamt)).replace("%player%", target.getName()));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.RemoveMessage").replace("%balance%", novecon2.formatMoney(depamt)).replace("%player%", target.getName()));
                            }
                        } catch(Exception e) {
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".PlayerNeverPlayed").replace("%player%", args[1]));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.PlayerNeverPlayed").replace("%player%", args[1]));
                            }
                        }
                    } else {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNovEconRemoveBalanceCommand"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNovEconRemoveBalanceCommand"));
                        }
                        return true;
                    }
                } else if(args[0].equalsIgnoreCase("reload")) {

                    if(!player.hasPermission("novecon2.admin.reload") && player.hasPermission("novecon2.admin.*")) {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".NoPermissions"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.NoPermissions"));
                        }
                        return true;
                    }

                    if(args.length == 1) {
                        try {
                            if(lang.get().contains(player.getLocale().toLowerCase())) {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".ReloadingConfiguration"));
                            } else {
                                player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.ReloadingConfiguration"));
                            }
                            boolean oldbool = novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank");
                            novecon2.getConfigModel().reload();
                            if(oldbool != novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                                if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                                    if(lang.get().contains(player.getLocale().toLowerCase())) {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".CreateDatabase"));
                                    } else {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.CreateDatabase"));
                                    }
                                    novecon2.getDatabaseModel().createCentralBank();
                                } else {
                                    if(lang.get().contains(player.getLocale().toLowerCase())) {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".RemoveDatabase"));
                                    } else {
                                        player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.RemoveDatabase"));
                                    }
                                    novecon2.getDatabaseModel().removeCentralBank();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if(lang.get().contains(player.getLocale().toLowerCase())) {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString(player.getLocale().toLowerCase() + ".InvalidNovEconReloadCommand"));
                        } else {
                            player.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.InvalidNovEconReloadCommand"));
                        }
                        return true;
                    }
                }
            }

        }

        return false;
    }
}
