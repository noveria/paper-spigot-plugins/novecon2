package me.linuxsquare.novecon2.model;

import me.linuxsquare.novecon2.controller.NovEcon2;
import me.linuxsquare.realbanks2.controller.RealBanks;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.math.BigDecimal;
import java.sql.*;
import java.time.Instant;
import java.util.HashMap;
import java.util.UUID;

public class DatabaseModel {

    private NovEcon2 novecon2;
    private LanguagesModel lang;

    private Connection con = null;
    private BukkitTask task;
    private BukkitTask correcTask;

    public DatabaseModel(NovEcon2 plugin) {
        novecon2 = plugin;
        lang = novecon2.getLanguagesModel();
    }

    public void connect() {
        try {
            if(novecon2.getConfigModel().getConf().getString("Database.type").equalsIgnoreCase("sqlite")) {
                File pluginFolder = new File("plugins" + System.getProperty("file.separator") + novecon2.getDescription().getName());
                if(!pluginFolder.exists()) {
                    pluginFolder.mkdir();
                }

                File dbfile = new File(pluginFolder + System.getProperty("file.separator") + "novecon2.db");

                if(!dbfile.exists() || dbfile.length() <= 0) {
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.YELLOW + "Database novecon2 not found. Creating...");
                    Class.forName("org.sqlite.JDBC");
                    setCon(DriverManager.getConnection("jdbc:sqlite:" + dbfile));
                    init();
                } else {
                    Class.forName("org.sqlite.JDBC");
                    setCon(DriverManager.getConnection("jdbc:sqlite:" + dbfile));
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.GREEN + "Database novecon2 found");
                }
            } else if(novecon2.getConfigModel().getConf().getString("Database.type").equalsIgnoreCase("mysql")) {
                String host = novecon2.getConfigModel().getConf().getString("Database.host");
                String port = novecon2.getConfigModel().getConf().getString("Database.port");
                String dbname = novecon2.getConfigModel().getConf().getString("Database.DBName");
                String username = novecon2.getConfigModel().getConf().getString("Database.username");
                String password = novecon2.getConfigModel().getConf().getString("Database.password");

                if(username.equalsIgnoreCase("<YourDBUserHere>") || username.equalsIgnoreCase("<YourDBPasswordHere>")) {
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.YELLOW + "Please update your database credentials and then restart your server.");
                    Bukkit.getPluginManager().disablePlugin(novecon2);
                    return;
                }

                String dburl = "jdbc:mysql://" + host + ":" + port;

                setCon(DriverManager.getConnection(dburl,username,password));

                String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '"+ dbname +"'";
                Statement stmt = getCon().createStatement();
                boolean exists = stmt.executeQuery(sql).next();
                if(!exists) {
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.YELLOW + "Creating Database " + dbname);
                    String sqlq = "CREATE DATABASE `"+ dbname +"`;";
                    stmt.execute(sqlq);
                } else {
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.GREEN + "Database "+dbname+" found. Hooking into it...");
                }
                getCon().close();

                dburl = "jdbc:mysql://" + host + ":" + port + "/" + dbname;

                Class.forName("com.mysql.jdbc.Driver");
                setCon(DriverManager.getConnection(dburl,username,password));
                exists = getCon().createStatement().executeQuery("SELECT * FROM information_schema.tables WHERE table_schema = '"+dbname+"' AND table_name = 'Accounts' LIMIT 1;").next();
                if(!exists) {
                    Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.YELLOW + "Initializing Database " + dbname);
                    init();
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            getCon().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void init() {
        try {
            Statement stmt = getCon().createStatement();

            String sql = "CREATE TABLE IF NOT EXISTS Accounts (uuid VARCHAR(40) PRIMARY KEY, balance DECIMAL(65,2) NOT NULL DEFAULT 0.00);";
            stmt.execute(sql);

            if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                createCentralBank();
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createCentralBank() {
        try {
            if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                if(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID").isEmpty()) {
                    novecon2.getBankUUIDModel().get().set("Settings.CentralBankUUID", UUID.randomUUID().toString());
                    novecon2.getBankUUIDModel().save();
                }
                String addBank = "INSERT INTO Accounts (uuid, balance) VALUES (?,?);";
                PreparedStatement pstmt = getCon().prepareStatement(addBank);
                pstmt.setString(1, novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID"));
                pstmt.setBigDecimal(2, BigDecimal.valueOf(novecon2.getConfigModel().getConf().getDouble("Settings.CentralBankCapital")));
                pstmt.executeUpdate();
                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeCentralBank() {
        try {
            if(!novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                String removeBank = "DELETE FROM Accounts WHERE uuid=?;";
                PreparedStatement pstmt = getCon().prepareStatement(removeBank);
                pstmt.setString(1, novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID"));
                pstmt.executeUpdate();
                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createAccountRecord(UUID puid) {
        try {
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uuid FROM Accounts WHERE uuid = '" + puid + "';");
            if(!rs.next()) {
                PreparedStatement pstmt = getCon().prepareStatement("INSERT INTO Accounts (uuid) VALUES (?);");
                pstmt.setString(1, puid.toString());
                pstmt.executeUpdate();
                pstmt.close();
            }
            stmt.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean removeAccountRecord(UUID puid) {
        try {
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uuid FROM Accounts WHERE uuid = '" + puid + "';");
            if(rs.next()) {
                String sql = "DELETE FROM Accounts WHERE uuid = ?";
                PreparedStatement pstmt = getCon().prepareStatement(sql);
                pstmt.setString(1, puid.toString());
                pstmt.executeUpdate();
                pstmt.close();
            } else {
                stmt.close();
                return true;
            }
            stmt.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public double getBalance(UUID pUID) {
        BigDecimal balance = new BigDecimal("0.00");

        try {
            PreparedStatement pstmt = getCon().prepareStatement("SELECT balance FROM Accounts WHERE uuid = ?;");
            pstmt.setString(1, pUID.toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                balance = rs.getBigDecimal("balance");
            }

            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return balance.doubleValue();
    }

    public double withdrawPlayer(UUID pUID, double amount) {
        double balance = getBalance(pUID);

        if(getBalance(pUID) >= amount) {
            balance -= amount;
            try {
                PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
                pstmt.setDouble(1, novecon2.round(balance, 2));
                pstmt.setString(2, pUID.toString());
                pstmt.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
                double rbbalBrut = novecon2.getRBAPI().getDatabaseAPIModel().getMoney(pUID);
                double tax = ((amount / 100) * novecon2.getConfigModel().getConf().getInt("Settings.TaxRate"));
                if (rbbalBrut >= (amount + tax)) {
                    double rbbalNet = (rbbalBrut - (amount+tax));
                    Bukkit.getPlayer(pUID).sendMessage(NovEcon2.PREFIX + ChatColor.RED + "Tax: " + tax);
                    novecon2.getEconomyModel().bankDeposit(novecon2.getEconomyModel().getBanks().get(0), tax);
                    novecon2.getRBAPI().getDatabaseAPIModel().setMoney(pUID, rbbalNet);
                    return rbbalNet;
                } else {
                    Bukkit.getPlayer(pUID).sendMessage(NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money in your pocket and on your bank account!");
                    return 0.0;
                }
            } else {
                Bukkit.getPlayer(pUID).sendMessage(NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money!");
                return 0.0;
            }
        }
        return balance+1;
    }

    public double depositPlayer(UUID pUID, double amount) {
        double balance = getBalance(pUID) + amount;
        try {
            PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
            pstmt.setDouble(1, novecon2.round(balance,2));
            pstmt.setString(2, pUID.toString());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balance;
    }

    public HashMap<UUID, Double> getAllPlayerBalance() {
        HashMap<UUID, Double> pocketContent = new HashMap<>();

        String query = "SELECT * FROM Accounts;";
        try {
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                pocketContent.put(UUID.fromString(rs.getString(1)), rs.getDouble(2));
            }
            pocketContent.remove(UUID.fromString(novecon2.getEconomyModel().getBanks().get(0)));
            return pocketContent;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pocketContent;
    }

    public void setPlayer(UUID pUID, double amount) {
        try {
            PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
            pstmt.setDouble(1, amount);
            pstmt.setString(2, pUID.toString());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void resetPlayer(UUID pUID) {
        try {
            PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
            pstmt.setDouble(1, 0.00);
            pstmt.setString(2, pUID.toString());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkUUID(UUID puid) {
        try {
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uuid FROM Accounts WHERE uuid = '" + puid + "';");
            if(rs.next()) {
                stmt.close();
                return true;
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public double bankBalance(String bankUuid) {
        BigDecimal balance = new BigDecimal("0.00");

        try {
            PreparedStatement pstmt = getCon().prepareStatement("SELECT balance FROM Accounts WHERE uuid = ?;");
            pstmt.setString(1, bankUuid);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                balance = rs.getBigDecimal("balance");
            }

            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return balance.doubleValue();
    }

    public double bankWidthdraw(String bankUuid, double amount) {
        double balance = novecon2.getEconomyModel().bankBalance(bankUuid).balance;

        if(balance >= amount) {
            balance -= amount;
            try {
                PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
                pstmt.setDouble(1, novecon2.round(balance,2));
                pstmt.setString(2, bankUuid);
                pstmt.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return balance;
    }

    public double bankDeposit(String bankUuid, double amount) {
        double balance = novecon2.getEconomyModel().bankBalance(bankUuid).balance + amount;
        try {
            PreparedStatement pstmt = getCon().prepareStatement("UPDATE Accounts SET balance = ? WHERE uuid = ?");
            pstmt.setDouble(1, novecon2.round(balance,2));
            pstmt.setString(2, bankUuid);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balance;
    }

    public void bankCorrector() {
        correcTask = Bukkit.getScheduler().runTaskTimerAsynchronously(novecon2, () -> {

            Double allPlayerMoney = 0.0;

            HashMap<UUID, Double> pocketContents = getAllPlayerBalance();
            HashMap<UUID, Double> bankContents = new HashMap<>();

            if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
                bankContents = novecon2.getRBAPI().getDatabaseAPIModel().getAllMoney();
            }

            final long daySeconds = 86400;
            long inactiveDuration = novecon2.getConfigModel().getConf().getInt("Settings.InactiveDayCount")*daySeconds;


            for(UUID pUID : pocketContents.keySet()) {
                long today = Instant.now().getEpochSecond();
                OfflinePlayer op = Bukkit.getOfflinePlayer(pUID);

                // If Player is inactive, remove from HashMap (freeze)
                if(today - op.getLastSeen() >= inactiveDuration) {
                    pocketContents.remove(pUID);
                    continue;
                }
                allPlayerMoney+=pocketContents.get(pUID);
            }

            if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
                for(UUID pUID : bankContents.keySet()) {
                    long today = Instant.now().getEpochSecond();
                    OfflinePlayer op = Bukkit.getOfflinePlayer(pUID);

                    // same as above
                    if(today - op.getLastSeen() >= inactiveDuration) {
                        bankContents.remove(pUID);
                        continue;
                    }
                    allPlayerMoney+=bankContents.get(pUID);
                }
            }
            double setBankBalance = novecon2.getConfigModel().getConf().getDouble("Settings.CentralBankCapital");
            double actualBankBalance = novecon2.getEconomyModel().bankBalance(novecon2.getEconomyModel().getBanks().get(0)).balance;

            if(setBankBalance != actualBankBalance+allPlayerMoney) {
                // Correct Up
                if(setBankBalance > actualBankBalance+allPlayerMoney) {
                    double difference = setBankBalance-(actualBankBalance+allPlayerMoney);
                    novecon2.getEconomyModel().bankDeposit(novecon2.getEconomyModel().getBanks().get(0), difference);
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(p.hasPermission("novecon2.admin.correction")) {
                            if(!novecon2.formatMoney(difference).contains("0.00")) {
                                if(lang.get().contains(p.getLocale().toLowerCase())) {
                                    p.sendMessage(NovEcon2.PREFIX + lang.get().getString(p.getLocale().toLowerCase() + ".admin.CorrectUp").replace("%balance%", novecon2.formatMoney(difference)));
                                } else {
                                    p.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.admin.CorrectUp").replace("%balance%", novecon2.formatMoney(difference)));
                                }
                            }
                        }
                    }
                }
                // Correct Down
                if(setBankBalance < actualBankBalance+allPlayerMoney) {
                    double difference = (actualBankBalance+allPlayerMoney)-setBankBalance;
                    novecon2.getEconomyModel().bankWithdraw(novecon2.getEconomyModel().getBanks().get(0), difference);
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(p.hasPermission("novecon2.admin.correction")) {
                            if(!novecon2.formatMoney(difference).contains("0.00")) {
                                if(lang.get().contains(p.getLocale().toLowerCase())) {
                                    p.sendMessage(NovEcon2.PREFIX + lang.get().getString(p.getLocale().toLowerCase() + ".admin.CorrectDown").replace("%balance%", novecon2.formatMoney(difference)));
                                } else {
                                    p.sendMessage(NovEcon2.PREFIX + lang.get().getString("en_gb.admin.CorrectDown").replace("%balance%", novecon2.formatMoney(difference)));
                                }
                            }
                        }
                    }
                }
            }

        }, 0, (20*60)*novecon2.getConfigModel().getConf().getInt("Settings.CentralBankCorrectInterval"));
    }

    public void stopBankCorrector() {
        Bukkit.getScheduler().cancelTask(correcTask.getTaskId());
    }

    public void DatabaseRefresher() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(novecon2, () -> {
            try {
                String sql = "SELECT * FROM Accounts LIMIT 1;";
                Statement stmt = getCon().createStatement();
                stmt.execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }, 0 , 20*30);
    }

    public void stopDBRefresher() {
        Bukkit.getScheduler().cancelTask(task.getTaskId());
    }


    public void setCon(Connection con) {
        this.con = con;
    }

    public Connection getCon() {
        return this.con;
    }

}
