package me.linuxsquare.novecon2.model;

import me.linuxsquare.novecon2.controller.NovEcon2;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EconomyModel implements Economy {

    private NovEcon2 novecon2;

    public EconomyModel(NovEcon2 plugin) {
        novecon2 = plugin;
    }


    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return novecon2.getName();
    }

    // Checks if CentralBank Feature is activated or not
    @Override
    public boolean hasBankSupport() {
        return novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank");
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    @Override
    public String format(double amount) {
        return novecon2.formatMoney(amount);
    }

    @Override
    public String currencyNamePlural() {
        return novecon2.getConfigModel().getConf().getString("Settings.currency");
    }

    @Override
    public String currencyNameSingular() {
        return novecon2.getConfigModel().getConf().getString("Settings.currency");
    }

    @Override
    public boolean hasAccount(String playerName) {
        return false;
    }

    @Override
    public boolean hasAccount(OfflinePlayer player) {
        return false;
    }

    @Override
    public boolean hasAccount(String playerName, String worldName) {
        return false;
    }

    @Override
    public boolean hasAccount(OfflinePlayer player, String worldName) {
        return false;
    }

    @Override
    public double getBalance(String playerName) {
        Player player = Bukkit.getPlayer(playerName);
        UUID uuid = player.getUniqueId();
        return novecon2.getDatabaseModel().getBalance(uuid);
    }

    @Override
    public double getBalance(OfflinePlayer player) {
        UUID uuid = player.getUniqueId();
        return novecon2.getDatabaseModel().getBalance(uuid);
    }

    @Override
    public double getBalance(String playerName, String world) {
        Player player = Bukkit.getPlayer(playerName);
        UUID uuid = player.getUniqueId();
        return novecon2.getDatabaseModel().getBalance(uuid);
    }

    @Override
    public double getBalance(OfflinePlayer player, String world) {
        UUID uuid = player.getUniqueId();
        return novecon2.getDatabaseModel().getBalance(uuid);
    }

    @Override
    public boolean has(String playerName, double amount) {
        if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
            if(getBalance(playerName) >= amount  || novecon2.getRBAPI().getDatabaseAPIModel().getMoney(Bukkit.getPlayerUniqueId(playerName)) >= amount) {
                return true;
            }
        }
        if(getBalance(playerName) >= amount) {
            return true;
        }
        return false;
    }

    @Override
    public boolean has(OfflinePlayer player, double amount) {
        if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
            if(getBalance(player) >= amount  || novecon2.getRBAPI().getDatabaseAPIModel().getMoney(player.getUniqueId()) >= amount) {
                return true;
            }
        }
        if(getBalance(player) >= amount) {
            return true;
        }
        return false;
    }

    @Override
    public boolean has(String playerName, String worldName, double amount) {
        if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
            if(getBalance(playerName) >= amount  || novecon2.getRBAPI().getDatabaseAPIModel().getMoney(Bukkit.getPlayerUniqueId(playerName)) >= amount) {
                return true;
            }
        }
        if(getBalance(playerName) >= amount) {
            return true;
        }
        return false;
    }

    @Override
    public boolean has(OfflinePlayer player, String worldName, double amount) {
        if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {
            if(getBalance(player) >= amount  || novecon2.getRBAPI().getDatabaseAPIModel().getMoney(player.getUniqueId()) >= amount) {
                return true;
            }
        }
        if(getBalance(player) >= amount) {
            return true;
        }
        return false;
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, double amount) {
        if (amount < 0) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Cannot withdraw negative funds");
        }
        if(Bukkit.getPlayer(playerName) == null) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Invalid Player");
        }
        try {
            Player player = Bukkit.getPlayer(playerName);
            UUID uuid = player.getUniqueId();
            double newbal = novecon2.getDatabaseModel().withdrawPlayer(uuid, amount);
            if(newbal == 0.0) {
                return new EconomyResponse(0, 0,EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money in your pocket and on your bank account!");
            }
            return new EconomyResponse(amount, newbal-1, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
        if (amount < 0) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Cannot withdraw negative funds");
        }
        try {
            UUID uuid = player.getUniqueId();
            double newbal = novecon2.getDatabaseModel().withdrawPlayer(uuid, amount);
            if(newbal == 0.0) {
                return new EconomyResponse(0, 0,EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money in your pocket and on your bank account!");
            }
            //FIXME: OK, NOW RETURNING ALWAYS FAILURE
            return new EconomyResponse(amount, newbal-1, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            e.printStackTrace();
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
        if (amount < 0) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Cannot withdraw negative funds");
        }
        if(Bukkit.getPlayer(playerName) == null) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Invalid Player");
        }
        try {
            Player player = Bukkit.getPlayer(playerName);
            UUID uuid = player.getUniqueId();
            double newbal = novecon2.getDatabaseModel().withdrawPlayer(uuid, amount);
            if(newbal == 0.0) {
                return new EconomyResponse(0, 0,EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money in your pocket and on your bank account!");
            }
            return new EconomyResponse(amount, newbal-1, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, String worldName, double amount) {
        if (amount < 0) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Cannot withdraw negative funds");
        }
        try {
            UUID uuid = player.getUniqueId();
            double newbal = novecon2.getDatabaseModel().withdrawPlayer(uuid, amount);
            if(newbal == 0.0) {
                return new EconomyResponse(0, 0,EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + ChatColor.RED + "You don't have enough money in your pocket and on your bank account!");
            }
            return new EconomyResponse(amount, newbal-1, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, double amount) {
        Player player = Bukkit.getPlayer(playerName);
        UUID uuid = player.getUniqueId();
        double newbal = novecon2.getDatabaseModel().depositPlayer(uuid, amount);

        try {
            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
        UUID uuid = player.getUniqueId();
        double newbal = novecon2.getDatabaseModel().depositPlayer(uuid, amount);

        try {
            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
        Player player = Bukkit.getPlayer(playerName);
        UUID uuid = player.getUniqueId();
        double newbal = novecon2.getDatabaseModel().depositPlayer(uuid, amount);

        try {
            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, String worldName, double amount) {
        UUID uuid = player.getUniqueId();
        double newbal = novecon2.getDatabaseModel().depositPlayer(uuid, amount);

        try {
            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse createBank(String name, String player) {
        return null;
    }

    @Override
    public EconomyResponse createBank(String name, OfflinePlayer player) {
        return null;
    }

    @Override
    public EconomyResponse deleteBank(String name) {
        return null;
    }

    @Override
    public EconomyResponse bankBalance(String bankUuid) {
        return new EconomyResponse(0, novecon2.getDatabaseModel().bankBalance(bankUuid), EconomyResponse.ResponseType.SUCCESS, null);
    }

    @Override
    public EconomyResponse bankHas(String name, double amount) {
        return null;
    }

    @Override
    public EconomyResponse bankWithdraw(String bankUuid, double amount) {
        if(amount < 0) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + "Cannot withdraw negative funds");
        }
        try {
            double newbal = novecon2.getDatabaseModel().bankWidthdraw(bankUuid, amount);

            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse bankDeposit(String bankUuid, double amount) {
        double newbal = novecon2.getDatabaseModel().bankDeposit(bankUuid, amount);

        try {
            return new EconomyResponse(amount, newbal, EconomyResponse.ResponseType.SUCCESS, null);
        } catch(Exception e) {
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, NovEcon2.PREFIX + e.getMessage());
        }
    }

    @Override
    public EconomyResponse isBankOwner(String name, String playerName) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String name, OfflinePlayer player) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String name, String playerName) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String name, OfflinePlayer player) {
        return null;
    }

    // Returns the UUID of the CentralBank
    @Override
    public List<String> getBanks() {
        List<String> banks = new ArrayList<String>();
        banks.add(novecon2.getBankUUIDModel().get().getString("Settings.CentralBankUUID"));
        return banks;
    }

    @Override
    public boolean createPlayerAccount(String playerName) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(String playerName, String worldName) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player, String worldName) {
        return false;
    }
}
