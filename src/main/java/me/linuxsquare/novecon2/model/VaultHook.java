package me.linuxsquare.novecon2.model;

import me.linuxsquare.novecon2.controller.NovEcon2;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.ServicePriority;

public class VaultHook {

    private NovEcon2 novecon2;
    private Economy provider;

    public VaultHook(NovEcon2 plugin) {
        novecon2 = plugin;
    }

    public void hook() {
        provider = novecon2.getEconomyModel();
        Bukkit.getServicesManager().register(Economy.class, this.provider, this.novecon2, ServicePriority.Highest);
        Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.GREEN + "VaultAPI hooked into " + this.novecon2.getName());
    }

    public void unhook() {
        Bukkit.getServicesManager().unregister(Economy.class, this.provider);
        Bukkit.getConsoleSender().sendMessage(NovEcon2.PREFIX + ChatColor.YELLOW + "VaultAPI unhooked " + this.novecon2.getName());
    }
}
