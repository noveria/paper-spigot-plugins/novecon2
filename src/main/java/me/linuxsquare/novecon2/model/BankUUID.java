package me.linuxsquare.novecon2.model;

import me.linuxsquare.novecon2.controller.NovEcon2;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class BankUUID {

    private NovEcon2 novecon2;

    private File file;
    private FileConfiguration conf;

    public BankUUID(NovEcon2 plugin) {
        novecon2 = plugin;
    }

    public void loadConfig() {
        File pluginFolder = new File("plugins" + System.getProperty("file.separator") + novecon2.getDescription().getName());

        file = new File(pluginFolder, "bankuuid.yml");

        if(!file.exists()) {
            novecon2.saveResource(file.getName(), true);
        }
        conf = YamlConfiguration.loadConfiguration(file);
    }

    public void save() {
        try {
            conf.save(file);
        } catch (IOException e) {
            novecon2.getLogger().warning("Unable to save " + file.getName());
        }
    }

    public FileConfiguration get() {
        return conf;
    }

    public void reload() {
        conf = YamlConfiguration.loadConfiguration(file);
    }
}
