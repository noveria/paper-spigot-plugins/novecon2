package me.linuxsquare.novecon2.listener;

import me.linuxsquare.novecon2.controller.NovEcon2;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.UUID;

public class PlayerJoinListener implements Listener {

    private NovEcon2 novecon2;

    public PlayerJoinListener(NovEcon2 plugin) {
        novecon2 = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent e) {
        OfflinePlayer op = e.getPlayer();
        novecon2.getDatabaseModel().createAccountRecord(op.getUniqueId());

        // Check if player hasn't played before
        if(!op.hasPlayedBefore()) {
            // If true, amount of money will be withdrawn from CentralBank
            if(novecon2.getConfigModel().getConf().getBoolean("Settings.CentralBank")) {
                novecon2.getEconomyModel().bankWithdraw(novecon2.getEconomyModel().getBanks().get(0), novecon2.getConfigModel().getConf().getDouble("Settings.startermoney"));
            }
            // if true, startermoney gets paid on RealBanks account, otherwise in pocket.
            if(novecon2.getConfigModel().getConf().getBoolean("Settings.RealBanksAPI")) {

                // Mandatory, because NovEcon2 gets loaded before RealBanks2,
                // but RealBanks has to create the record first, before paying the starter amount.
                Bukkit.getScheduler().runTaskLaterAsynchronously(novecon2, () -> {
                    Double bal = novecon2.getRBAPI().getDatabaseAPIModel().getMoney(op.getUniqueId());
                    novecon2.getRBAPI().getDatabaseAPIModel().setMoney(op.getUniqueId(), bal+novecon2.getConfigModel().getConf().getDouble("Settings.startermoney"));
                }, 40);
            } else {
                novecon2.getEconomyModel().depositPlayer(op, novecon2.getConfigModel().getConf().getDouble("Settings.startermoney"));
            }
        }
    }
}
